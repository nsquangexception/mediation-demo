//
//  AerServCustomEventInterstitial.m
//
//  Copyright (c) 2015 AerServ. All rights reserved.
//

#import "AerServCustomEventInterstitial.h"
#import "AerServSDK/ASAdView.h"
#import "AerServSDK/ASInterstitialViewController.h"
#import <AerServSDK/AerServSDK.h>


@interface AerServCustomEventInterstitial () <ASInterstitialViewControllerDelegate>

@property (nonatomic, strong) ASInterstitialViewController *asInterstitial;

@end


@implementation AerServCustomEventInterstitial

- (void)requestInterstitialWithCustomEventInfo:(NSDictionary *)info {
    // Instantiate ad
    NSString *placement = [info objectForKey:@"placement_id"];
    NSString* appId = [info objectForKey:@"app_id"];
    [AerServSDK initializeWithAppID:appId];
    self.asInterstitial = [ASInterstitialViewController viewControllerForPlacementID:placement withDelegate:self];
    
    // Set optional timeout parameter
    NSTimeInterval timeoutMillis = [[info objectForKey:@"timeoutMillis"] integerValue];
    if (timeoutMillis) {
        self.asInterstitial.timeoutInterval = timeoutMillis/1000;
    }
    
    // Set optional keywords parameter
    id keywords = [info objectForKey:@"keywords"];
    if([keywords isKindOfClass:[NSArray class]]) {
        self.asInterstitial.keyWords = (NSArray *)keywords;
    } else if([keywords isKindOfClass:[NSString class]]) {
        NSString *keywordsJsonStr = (NSString *)keywords;
        NSError *error;
        id keywordObjects = [NSJSONSerialization JSONObjectWithData:[keywordsJsonStr dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
        if (error) {
            NSLog(@"Error reading keyword list: %@", error);
        } else {
            self.asInterstitial.keyWords = keywordObjects;
        }
    }
    
    [self.asInterstitial loadAd];
}

- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    [self.asInterstitial showFromViewController:rootViewController];
}

- (void)interstitialViewControllerAdLoadedSuccessfully:(ASInterstitialViewController *)viewController {
    [self.delegate interstitialCustomEvent:self didLoadAd:self];
}

- (void)intersitialViewControllerAdFailedToLoad:(ASInterstitialViewController *)viewController withError:(NSError *)error {
    [self.delegate interstitialCustomEvent:self didFailToLoadAdWithError:error];
    
}

- (void)intersitialViewControllerWillAppear:(ASInterstitialViewController *)viewController {
    [self.delegate interstitialCustomEventWillAppear:self];
}

- (void)intersitialViewControllerDidAppear:(ASInterstitialViewController *)viewController {
    [self.delegate interstitialCustomEventDidAppear:self];
}

- (void)interstitialViewControllerWillDisappear:(ASInterstitialViewController *)viewController {
    [self.delegate interstitialCustomEventWillDisappear:self];
}

- (void)interstitialViewControllerDidDisappear:(ASInterstitialViewController *)viewController {
    [self.delegate interstitialCustomEventDidDisappear:self];
}

- (void)interstitialViewControllerAdWasTouched:(ASInterstitialViewController *)viewController {
    [self.delegate interstitialCustomEventDidReceiveTapEvent:self];
}

- (void)dealloc {
    self.asInterstitial = nil;
}
- (void)interstitialViewControllerDidPreloadAd:(ASInterstitialViewController *)viewController{
    [self.delegate interstitialCustomEventWillAppear:self];
}

@end
