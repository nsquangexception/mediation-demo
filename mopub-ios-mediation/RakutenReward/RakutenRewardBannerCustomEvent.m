//
//  RakutenRewardBannerCustomEvent.m
//  MapSearch
//
//  Created by Ha, Quang on 2018/09/13.
//

#import "RakutenRewardBannerCustomEvent.h"
#import <RakutenRewardSDK/RakutenRewardSDK-Swift.h>
@interface RakutenRewardBannerCustomEvent () <RakutenADBannerViewDelegate>
@property RakutenADBannerView * bannerView;
@end

@implementation RakutenRewardBannerCustomEvent
- (instancetype)init
{
    self = [super init];
    if (self) {
        _bannerView = [[RakutenADBannerView alloc] initWithFrame:CGRectMake(0,0,320,50)];
        [_bannerView setDelegate:self];
        [_bannerView setLocationId:@"Do I neet location ID to show ad???"];
    }
    return self;
}
- (void)requestAdWithSize:(CGSize)size customEventInfo:(NSDictionary *)info {
    [_bannerView loadWithAdSize:RAdSizeBanner];
}
- (void)didFailToReceiveAdWithBannerView:(RakutenADBannerView * _Nonnull)bannerView message:(NSString * _Nonnull)message code:(enum RADErrorCode)code {
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:message forKey:NSLocalizedDescriptionKey];
    NSError* error = [[NSError alloc] initWithDomain:NSStringFromClass([self class]) code:code userInfo:details];
    [self.delegate bannerCustomEvent:self didFailToLoadAdWithError:error];
}

- (void)didReceiveAdWithBannerView:(RakutenADBannerView * _Nonnull)bannerView {
    [self.delegate bannerCustomEvent:self didLoadAd:bannerView];
}



@end
