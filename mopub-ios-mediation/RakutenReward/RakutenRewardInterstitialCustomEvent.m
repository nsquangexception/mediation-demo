//
//  RakutenRewardInterstitialCustomEvent.m
//  MapSearch
//
//  Created by Ha, Quang on 2018/09/06.
//

#import "RakutenRewardInterstitialCustomEvent.h"
#import <RakutenRewardSDK/RakutenRewardSDK.h>
#import <RakutenRewardSDK/RakutenRewardSDK-Swift.h>

@interface RakutenRewardInterstitialCustomEvent () <RakutenADInterstitialDelegate>
@property RakutenADInterstitial * interstitial;
@end

@implementation RakutenRewardInterstitialCustomEvent
- (void)requestInterstitialWithCustomEventInfo:(NSDictionary *)info {
    _interstitial = [[RakutenADInterstitial alloc] instantiate];
    [_interstitial setDelegate:self];
    [self.delegate interstitialCustomEvent:self didLoadAd:nil];
}
- (void)showInterstitialFromRootViewController:(UIViewController *)rootViewController {
    //[_interstitial setRootViewController:rootViewController];
    _interstitial.locationId = @"123";
    _interstitial.rootViewController = rootViewController;
    [_interstitial loadAd];
}



- (void)interstitialDidFailToReceiveAdWithAd:(RakutenADInterstitial * _Nonnull)ad message:(NSString * _Nonnull)message code:(enum RADErrorCode)code {
    [self.delegate interstitialCustomEvent:self didFailToLoadAdWithError:nil];
}

- (void)interstitialDidReceiveAdWithAd:(RakutenADInterstitial * _Nonnull)ad {
    
}
- (void)interstitialWillPresentScreenWithAd:(RakutenADInterstitial *)ad {
    [self.delegate interstitialCustomEventWillAppear:self];
}
- (void)interstitialWillDismissScreenWithAd:(RakutenADInterstitial *)ad {
    [self.delegate interstitialCustomEventWillDisappear:self];
    [self.delegate interstitialCustomEventDidDisappear:self];
}
- (void)interstitialWillLeaveApplicationWithAd:(RakutenADInterstitial *)ad {
    [self.delegate interstitialCustomEventDidReceiveTapEvent:self];
    [self.delegate interstitialCustomEventWillLeaveApplication:self];
}

@end
