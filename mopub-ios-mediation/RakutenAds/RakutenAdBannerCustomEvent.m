//
//  RakutenAdBannerCustomEvent.m
//  MapSearch
//
//  Created by Ha, Quang on 2019/01/09.
//

#import "RakutenAdBannerCustomEvent.h"
#import <RsspSDK/RsspSDK.h>
@interface RakutenAdBannerCustomEvent()
@property (nonatomic, strong) RSPAdView * banner;
@end
@implementation RakutenAdBannerCustomEvent
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.banner = [RSPAdView new];
    }
    return self;
}
- (void)requestAdWithSize:(CGSize)size customEventInfo:(NSDictionary *)info {
    NSString* spotId = [info objectForKey:@"spotId"];
    [self.banner setAdSpotId:spotId];
    [self.banner showWithEventHandler:^BOOL(RSPAdViewEvent event){
        switch (event) {
            case RSP_ADVIEW_EVENT_SUCCEEDED:
                NSLog(@"received event succeeded");
                [self.delegate bannerCustomEvent:self didLoadAd:self.banner];
                break;
            case RSP_ADVIEW_EVENT_FAILED:
                NSLog(@"received event failed");
                [self.delegate bannerCustomEvent:self didFailToLoadAdWithError:nil];
                break;
            case RSP_ADVIEW_EVENT_CLICKED:
                NSLog(@"received event clicked");
                [self.delegate bannerCustomEventWillLeaveApplication:self];
                break;
            default:
                NSLog(@"unknown event");
        }
        return YES;
    }];
    
}
@end
