/*
 Copyright (C) 2017 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 The application delegate class used for installing our navigation controller.
 */

#import "AppDelegate.h"
#import "MoPub.h"
#import "Constant.h"
#import "MPGoogleGlobalMediationSettings.h"
#import <RakutenRewardSDK/RakutenRewardSDK.h>
@interface AppDelegate () <RakutenRewardDelegate>

@end

@implementation AppDelegate

// The app delegate must implement the window @property
// from UIApplicationDelegate @protocol to use a main storyboard file.
@synthesize window;
- (void)applicationDidFinishLaunching:(UIApplication *)application {
    MPMoPubConfiguration *sdkConfig = [[MPMoPubConfiguration alloc] initWithAdUnitIdForAppInitialization:mpIntAdID];
//    MPGoogleGlobalMediationSettings *mediationSettings = [[MPGoogleGlobalMediationSettings alloc] init];
//    mediationSettings.npa = @"1";
//    sdkConfig.globalMediationSettings = [[NSArray alloc] initWithObjects:mediationSettings, nil];
    

    [[MoPub sharedInstance] initializeSdkWithConfiguration:sdkConfig completion: ^(void){
        //[[MPInterstitialAdController interstitialAdControllerForAdUnitId:mpIntAdID] loadAd];
    }];
    [[RakutenReward sharedInstance] setDelegate:self];
    [[RakutenReward sharedInstance] startSessionWithAppCode:@"anAuY28ucmFrdXRlbi5yZXdhcmQuc2FtcGxlLmlvcy1vbXpRMTlPdjNmQzlPenJEZUQyd2FENXdkaENKdkVDNg=="];
}

- (void)didSDKStateChange:(enum RakutenRewardStatus)status {
    
}

@end
