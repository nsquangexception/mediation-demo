/*
 Copyright (C) 2017 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 Secondary view controller used to display the map and found annotations.
 */

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MPAdView.h"

@interface MapViewController : UIViewController

@property (nonatomic, strong) NSArray *mapItemList;
@property (nonatomic, assign) MKCoordinateRegion boundingRegion;
@property MPAdView * adView;

@end
