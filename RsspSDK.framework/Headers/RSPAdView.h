//
//  RSPAdView.h
//  RSPSDK
//
//  Created by Wu Wei on 2018/07/23.
//  Copyright © 2018 LOB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, RSPAdViewPosition) {
    RSP_ADVIEW_POSITION_CUSTOM,
    RSP_ADVIEW_POSITION_TOP,
    RSP_ADVIEW_POSITION_BOTTOM,
    RSP_ADVIEW_POSITION_TOP_LEFT,
    RSP_ADVIEW_POSITION_TOP_RIGHT,
    RSP_ADVIEW_POSITION_BOTTOM_LEFT,
    RSP_ADVIEW_POSITION_BOTTOM_RIGHT,
};

typedef NS_ENUM(NSUInteger, RSPAdViewEvent) {
    RSP_ADVIEW_EVENT_SUCCEEDED,
    RSP_ADVIEW_EVENT_FAILED,
    RSP_ADVIEW_EVENT_CLICKED,
};

@class RSPAdView;

@interface RSPAdView : UIView

@property(nonatomic, copy, nonnull) NSString* adSpotId;

-(void) show;
-(void) showWithEventHandler:(nullable BOOL (^)(RSPAdViewEvent event)) handler;
-(void) setPosition:(RSPAdViewPosition)position inView:(UIView*) parentView;

@end
